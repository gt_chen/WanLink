// pages/home/home.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgUrls: [
      // 'http://www.wanlink.com.cn/wl/images/slide1.jpeg',
      // 'http://www.wanlink.com.cn/wl/images/slide2.jpeg',
      // 'http://www.wanlink.com.cn/wl/images/slide3.jpeg',
      // 'http://www.wanlink.com.cn/wl/images/slide4.jpeg'
      '/images/swiper1.jpg',
      '/images/swiper2.jpg',
      '/images/swiper3.jpg',
      '/images/swiper4.jpg'

    ],
    indicatorDots: true,
    indicatorColor: "#FFFFFF",
    indicatorActiveColor:"#FFA500",//与上属性无效
    autoplay: true,
    interval: 4000,//默认5000
    duration: 500,//默认值
    circular:true
  },

  changeIndicatorDots: function () {
    this.setData({
      indicatorDots: !this.data.indicatorDots
    })
  },
  
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    wx.setNavigationBarTitle({
      title: '首页',
      success: function(res) {},
      fail: function(res) {},
      complete: function(res) {},
    }),
    wx.setNavigationBarColor({
      frontColor: '#000000',
      backgroundColor: '#FFA500',
      animation: {},
      success: function(res) {},
      fail: function(res) {},
      complete: function(res) {},
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})